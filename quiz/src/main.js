(function()
{
    require("./style.css");

    var Vue = require("vue");
    var app = new Vue({
        el: "#app",
        template: "<MainPage />",
        components: {
            MainPage: require("./pages/MainPage.vue").default
        }
    });
})();