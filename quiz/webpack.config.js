const path = require("path");
const webpack = require("webpack");
const VueLoaderPlugin = require("vue-loader/lib/plugin");

module.exports = (env, argv) => {
    return {
        entry: "./src/main.js",
        output: {
            path: path.resolve(__dirname),
            filename: "dist/js/main.js"
        },
        resolve: {
            alias: {
                "vue$": "vue/dist/vue"
            }
        },
        mode: argv.mode,
        plugins: [
            new webpack.DefinePlugin({
                "process.env": {
                    NODE_ENV: JSON.stringify(argv.mode),
                    RAVEN_DNS: argv.mode === "production" ? JSON.stringify("https://930f114103ac44a6b0e4464f12afc99d@sentry.tboard.kr/4") : JSON.stringify("https://4da7142ded904a0cbe8c4fbaf53a311c@sentry.tboard.kr/2")
                }
            }),
            new VueLoaderPlugin()
        ],
        module: {
            rules: [{
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["babel-preset-env"]
                    }
                }
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"],
            },
            {
            test: /\.(jpg|png)/,
            loader: "file-loader?name=static/img/[name].[ext]",
            }]
        }
    };
};